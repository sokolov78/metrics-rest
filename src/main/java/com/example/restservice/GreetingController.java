package com.example.restservice;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class GreetingController {

	private final String MINKEY = "MIN_";
	private final String MAXKEY = "MAX_";
	private final Random r = new Random();
	AtomicInteger counter = new AtomicInteger(10);
	Map<String, Integer> mm = new HashMap<>();
	int minPredel = -10;
	int maxPredel = 10;
	int groupSize = 10;
	public Map<String, Integer> tickersIndexes = new HashMap<String, Integer>();
	
	@GetMapping("publish")
	@ResponseStatus(code = HttpStatus.OK)
	public String trade(@RequestParam String[] ids) {
		for (String iD : ids) {
			tickersIndexes.put(iD, counter.incrementAndGet());
		}
		
		return "subscribed :" + tickersIndexes.toString();
	}
	
	@GetMapping("metrics")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, String> metrics(@RequestParam String[] ids) {
	    return generateValues(ids);
	}
	
	private Map<String, String> generateValues(String[] ids){
		Map<String, String> list = new HashMap<>();
		for (String id : ids) {
			if (tickersIndexes.containsKey(id))
				list.put(id, nextRandomInt(-10, 10).toString());
		}
        return list;
		
	}


	@GetMapping("range-metrics")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Map<String, Integer>> getCategoriesList(@RequestParam String[] ids) {
		return generateData(ids);
	}

	private Map<String, Map<String, Integer>> generateData(String[] ids) {
		minPredel = (int)Math.round(counter.get()/groupSize) * groupSize * (-1);
		maxPredel = (int)Math.round(counter.get()/groupSize) * groupSize;
		Map<String, Map<String, Integer>> list = new HashMap<>();
		for (String id : ids) {
			if (tickersIndexes.containsKey(id)){
				Map<String, Integer> minMaxTemp = new HashMap<>();
				int val = nextRandomInt(minPredel, maxPredel);
				minMaxTemp.put("val", val);
				minMaxTemp.put("min", getMinbyId(id, val));
				minMaxTemp.put("max", getMaxbyId(id, val));
				list.put(id, minMaxTemp);
			}
		}
		
		counter.getAndIncrement();
		return list;

	}
	
	private int getMinbyId(String id, int val){
		if (!mm.containsKey(MINKEY+id)) mm.put(MINKEY+id, 0);
		mm.put(MINKEY+id,  Math.min(val, mm.get(MINKEY+id)));
		return mm.get(MINKEY+id);
	}
	
	private int getMaxbyId(String id, int val){
		if (!mm.containsKey(MAXKEY+id)) mm.put(MAXKEY+id, 0);
		mm.put(MAXKEY+id,  Math.max(val, mm.get(MAXKEY+id)));
		return mm.get(MAXKEY+id);
	}

	private Double nextRandom(int rangeMin, int rangeMax) {
		return (Double) (rangeMin + (rangeMax - rangeMin) * r.nextDouble());
	}

	private Integer nextRandomInt(int rangeMin, int rangeMax) {
		return ThreadLocalRandom.current().nextInt(rangeMin, rangeMax + 1);
	}

}
